﻿using System;
using System.Windows.Forms;

namespace Calculadora
{
    public enum Operacoes
    {
        Soma,
        Substracao,
        Multiplicacao,
        Divisao        
    }

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private Operacoes Operacao;
        private string PrimeiroValor="";
        private bool troca=false;
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btn1_Click(object sender, EventArgs e)
        {
            AdicionaDigito(1);
        }


        private void AdicionaDigito(int digito)
        {
            if (!troca)
            {
                txtEcra.Text = txtEcra.Text + digito;
                
            }
            else
            {
                troca = false;
                txtEcra.Text =  digito.ToString();
            }
            

        }

        private void btn2_Click(object sender, EventArgs e)
        {
            AdicionaDigito(2);
        }

        private void btnMais_Click(object sender, EventArgs e)
        {
            Operacao = Operacoes.Soma;
            PrimeiroValor = txtEcra.Text;
            troca = true;
        }

        private void btnIgual_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(PrimeiroValor)) { return; }
            switch (Operacao)
            {
                case Operacoes.Soma:
                    txtEcra.Text = (Convert.ToDouble(PrimeiroValor) + Convert.ToDouble(txtEcra.Text)).ToString();
                    break;
                case Operacoes.Substracao:
                    txtEcra.Text = (Convert.ToDouble(PrimeiroValor) - Convert.ToDouble(txtEcra.Text)).ToString();
                    break;
                case Operacoes.Multiplicacao:
                    txtEcra.Text = (Convert.ToDouble(PrimeiroValor) * Convert.ToDouble(txtEcra.Text)).ToString();
                    break;
                case Operacoes.Divisao:
                    txtEcra.Text = (Convert.ToDouble(PrimeiroValor)/ Convert.ToDouble(txtEcra.Text)).ToString();
                    break;
                default:
                    break;
            }
            troca = true;
        }

        private void btnMenos_Click(object sender, EventArgs e)
        {
            Operacao = Operacoes.Substracao;
            PrimeiroValor = txtEcra.Text;
            troca = true;
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Operacao = Operacoes.Multiplicacao;
            PrimeiroValor = txtEcra.Text;
            troca = true;

        }
        private void button20_Click(object sender, EventArgs e)
        {
            Operacao = Operacoes.Divisao;
            PrimeiroValor = txtEcra.Text;
            troca = true;
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            AdicionaDigito(3);
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            AdicionaDigito(4);
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            AdicionaDigito(5);
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            AdicionaDigito(6);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            AdicionaDigito(7);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            AdicionaDigito(8);
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            AdicionaDigito(9);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AdicionaDigito(0);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            txtEcra.Text = string.Empty;
            PrimeiroValor = string.Empty;

        }

        private void button18_Click(object sender, EventArgs e)
        {
            txtEcra.Text = txtEcra.Text.Substring(0,txtEcra.Text.Length - 1);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (txtEcra.Text.IndexOf(',') == -1)
            {
                txtEcra.Text = txtEcra.Text + ",";
            }            
        }
    }
}
